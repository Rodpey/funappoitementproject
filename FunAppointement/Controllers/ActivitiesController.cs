﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FunAppointement;
using FunAppointement.Models;

namespace FunAppointement.Controllers
{
    public class ActivitiesController : Controller
    {
        private FunAppointementDB db = new FunAppointementDB();

        // GET: Activities
        public async Task<ActionResult> Index()
        {
            var activities = db.Activities.Include(a => a.Group).Include(a => a.TypeActivity);
            return View(await activities.ToListAsync());
        }

        // GET: Activities/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = await db.Activities.FindAsync(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            return View(activity);
        }

        // GET: Activities/Create
        public ActionResult Create()
        {
            ViewBag.IDGroup = new SelectList(db.Groups, "IDGroup", "NameGroup");
           
            ViewBag.IDTypeActivity = new SelectList(db.TypeActivities, "IDTypeActivity", "Name");
            return View();
        }

        // POST: Activities/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDActivity,Plage,Date,Validated,IDTypeActivity,IDGroup")] Activity activity)
        {
            if (ModelState.IsValid)
            {
                db.Activities.Add(activity);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IDGroup = new SelectList(db.Groups, "IDGroup", "NameGroup", activity.IDGroup);
            
            ViewBag.IDTypeActivity = new SelectList(db.TypeActivities, "IDTypeActivity", "Name", activity.IDTypeActivity);
            return View(activity);
        }

        // GET: Activities/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = await db.Activities.FindAsync(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDGroup = new SelectList(db.Groups, "IDGroup", "NameGroup", activity.IDGroup);
           
            ViewBag.IDTypeActivity = new SelectList(db.TypeActivities, "IDTypeActivity", "Name", activity.IDTypeActivity);
            return View(activity);
        }

        // POST: Activities/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDActivity,Plage,Date,Validated,IDTypeActivity,IDGroup")] Activity activity)
        {
            if (ModelState.IsValid)
            {
                db.Entry(activity).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IDGroup = new SelectList(db.Groups, "IDGroup", "NameGroup", activity.IDGroup);
            
            ViewBag.IDTypeActivity = new SelectList(db.TypeActivities, "IDTypeActivity", "Name", activity.IDTypeActivity);
            return View(activity);
        }

        // GET: Activities/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = await db.Activities.FindAsync(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            return View(activity);
        }

        // POST: Activities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Activity activity = await db.Activities.FindAsync(id);
            db.Activities.Remove(activity);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
