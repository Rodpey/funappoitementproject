﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FunAppointement.Models;

namespace FunAppointement.Controllers
{
    public class TypeActivitiesController : Controller
    {
        private FunAppointementDB db = new FunAppointementDB();

        // GET: TypeActivities
        public async Task<ActionResult> Index()
        {
            return View(await db.TypeActivities.ToListAsync());
        }

        // GET: TypeActivities/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeActivity typeActivity = await db.TypeActivities.FindAsync(id);
            if (typeActivity == null)
            {
                return HttpNotFound();
            }
            return View(typeActivity);
        }

        // GET: TypeActivities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TypeActivities/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDTypeActivity,Name,MinUser,MaxUser")] TypeActivity typeActivity)
        {
            if (ModelState.IsValid)
            {
                db.TypeActivities.Add(typeActivity);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(typeActivity);
        }

        // GET: TypeActivities/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeActivity typeActivity = await db.TypeActivities.FindAsync(id);
            if (typeActivity == null)
            {
                return HttpNotFound();
            }
            return View(typeActivity);
        }

        // POST: TypeActivities/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDTypeActivity,Name,MinUser,MaxUser")] TypeActivity typeActivity)
        {
            if (ModelState.IsValid)
            {
                db.Entry(typeActivity).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(typeActivity);
        }

        // GET: TypeActivities/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeActivity typeActivity = await db.TypeActivities.FindAsync(id);
            if (typeActivity == null)
            {
                return HttpNotFound();
            }
            return View(typeActivity);
        }

        // POST: TypeActivities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TypeActivity typeActivity = await db.TypeActivities.FindAsync(id);
            db.TypeActivities.Remove(typeActivity);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
