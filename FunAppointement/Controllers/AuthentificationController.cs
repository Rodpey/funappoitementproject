﻿using FunAppointement.Models;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace WebApplication1.Controllers
{
    public class AuthentificationController : Controller
    {
        // GET: Authentification
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User user)
        {
            if (user.Pseudo != null || user.Password != null)
            {
                using (var context = new FunAppointementUsers())
                {
                    if (context.Users.Any(n => n.Pseudo == user.Pseudo && n.Password == user.Password))
                    {
                        FormsAuthentication.SetAuthCookie(user.IDUser.ToString(), true);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ViewBag.Message = "Identifiant/MDP incorrects";
                        return View("Login");
                    }
                }
            }
            else
            {
                ViewBag.Message = "Veuillez remplir les champs";
                return View("Login");
            }
        }
    }
}