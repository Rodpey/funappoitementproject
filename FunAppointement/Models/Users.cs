﻿using System.Data.Entity;

namespace FunAppointement.Models
{
    public partial class FunAppointementUsers : DbContext
    {
        public FunAppointementUsers()
            : base("name=Users")
        {
        }

        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .Property(e => e.Pseudo)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Password)
                .IsFixedLength();
        }
    }
}